import React from 'react';
import { createAppContainer } from 'react-navigation';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import { Icon } from 'native-base';
import AboutScreen from './Screens/AboutScreen'
import HomeScreen from './Screens/HomeScreen'
import ShopScreen from './Screens/ShopScreen'


const TabNavigator = createBottomTabNavigator({

  HomePage: {
    screen: HomeScreen,

    navigationOptions: {
      tabBarLabel:"Home Page",
      tabBarIcon: ( ) => (
        <Icon name="home" size={30} />
      )
    },
  },

  ShopPage: {
    screen: ShopScreen,

    navigationOptions: {
      tabBarLabel:"Shop",
      tabBarIcon: ( ) => (
        <Icon name="people" size={30} />
      )
    },
  },
  AboutPage: {
    screen: AboutScreen,

    navigationOptions: {
      tabBarLabel:"About Page",
      tabBarIcon: ( ) => (
        <Icon name="about" size={30} />
      )
    },
  }

  // Main: ShopScreen,
  // About: AboutScreen
  // Cart: CartScreen
});

export default createAppContainer(TabNavigator);