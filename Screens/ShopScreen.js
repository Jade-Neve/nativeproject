import { View , Text} from 'native-base';
import * as React from 'react';

class ShopScreen extends React.Component {
    render() {
      return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
          <Text>Check our products</Text>
        </View>
      );
    }
  }
  

export default ShopScreen;
