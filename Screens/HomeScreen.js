import { View , Text} from 'native-base';
import * as React from 'react';

class HomeScreen extends React.Component {
    render() {
      return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
          <Text>Home Screen
          </Text>
        </View>
      );
    }
  }
  

export default HomeScreen;
